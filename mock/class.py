from unittest.mock import patch

class MyClass(object):
    def __init__(self):
        print('Created MyClass@{0}'.format(id(self)))

def create_instance():
    return MyClass()

@patch('__main__.MyClass')
def create_instance2(MockClass1):
    MockClass1.return_value = 'foo'
    return create_instance()

print(create_instance2())
print(create_instance())