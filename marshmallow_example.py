from marshmallow import Schema, fields, pprint, post_load, ValidationError, validates

class Person(object):
    def __init__(self, name, age, email):
        self.name = name
        self.age = age
        self.email = email
    
    def __repr__(self):
        return f'{self.name} is {self.age} years old with email: {self.email}'

""" def validate_age(age):
    if age>100 or age <0:
        raise ValidationError('That is not a valid age') """

class PersonSchema(Schema):
    name = fields.String(required=True)
    age = fields.Integer() 
    email = fields.Email()

    @post_load
    def create_person(self, data):
        return Person(**data) # ** -> take the keys
    
    @validates('age')
    def validate_age(self,age):
        if age>100 or age <0:
           raise ValidationError('That is not a valid age')

""" input_dict = {}

input_dict['name'] = input('What is your name? ')
input_dict['age'] = input('Who old are you? ')
input_dict['email'] = input('Whats you email ') """

#person = Person(name = input_dict['name'], age = input_dict['age'])
schema = PersonSchema()

input_dict = {'name': 'Helder', 'age': '-2', 'email': 'ribeirh02@gmail.com'}
#result = schema.dump(person) #serialize person (simple python dict) 
result = schema.load(input_dict)

print(f'Errors: {result.errors}')
pprint(result.data)