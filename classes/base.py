from space.planets import Planet
from space.calc import planet_mass, planet_vol

naboo = Planet('Base',2000,5,'Base System')

naboo_mass = planet_mass(naboo.gravity, naboo.radius)
naboo_vol = planet_vol(naboo.radius)

print(f'Name: {naboo.name}')
print(f'Radius: {naboo.radius}')
print(f'Gravity: {naboo.gravity}')
print(f'System: {naboo.system}')
print(f'Mass: {naboo_mass:.2f}')
print(f'Volume: {naboo_vol:.2f}')