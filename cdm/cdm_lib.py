from marshmallow import Schema, fields, post_load, pprint
from typing import Optional, List

class Dish(object):
    """ Dish contains the list of receptor IDs to be used"""

    def __init__(self, receptorIDList: Optional[List[str]] = None):
        if receptorIDList is None:
            self.receptorIDList = []
        self.receptorIDList = []
        for receptorID in receptorIDList:
            self. receptorIDList.append(receptorID)

    def __repr__(self):
        return f'<Dish( receptorIDList = {self.receptorIDList})>'


class SubArray:
    """ A subarray has an id and a dish element that lists the receptor IDs"""

    def __init__(self, subarrayID: int, dish: Dish):
        # As a user-facing class, handle both strings and ints
        self.dish = dish
        self.subarrayID = subarrayID

    def __repr__(self):
        return f'<SubArray(subarrayID = {self.subarrayID}, dish = {self.dish})>'


class DishSchema(Schema):
    """ Schema used to convert the Person python object to Python """

    # dish consists of a list of receptorIds
    receptorIDList = fields.List(fields.Str())

    # this annotation defines any operations needed after load.
    # If we needed to do any work on the object we could do it here
    # in this case we are just treating each of the named arguments
    # we were passed to the equivalent named argument and letting
    # the __init__ do the work.
    @post_load
    def create_dish(self, data):
        return Dish(**data)


class SubArraySchema(Schema):
    """ Schema used to convert the SubArray between python and json"""
    subarrayID = fields.Integer()

    # contains a nested 'dish'
    dish = fields.Nested(DishSchema)

    # this annotation defines any operations needed after load.
    # If we needed to do any work on the object we could do it here
    # in this case we are just treating each of the named arguments
    # we were passed to the equivalent named argument and letting
    # the __init__ do the work.
    @post_load
    def create_subarray(self, data):
        return SubArray(**data)


def cdm_to_json(schema, obj):
    # convert the object into a json string. We could use schema.dump
    # if we only needed it as a dictionary representation
    result = schema.dumps(obj)
    return result.data


def cdm_to_obj(schema, json_str):
    # convert to a python object
    # assuming we get a string as input so eval to create the initial
    # dictionary representation
    json_dict = eval(json_str)
    result = schema.load(json_dict)
    return result.data
